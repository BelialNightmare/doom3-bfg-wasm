# Doom 3 BFG Edition Wasm

[Inspired By D3Wasm](http://www.continuation-labs.com/projects/d3wasm/#author-and-contact)

## Disclaimer
Although this is inspired by D3Wasm, I will not be using any code from that project
and will be providing my own port. I was blown away by the project and I am looking
to make my own version. I am also in no way associated with Id Software and will
not be monetising any of this code.

