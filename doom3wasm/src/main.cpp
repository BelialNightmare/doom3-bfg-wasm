#include <functional>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#include <SDL.h>
#include <SDL_opengles2.h>

// Shader sources
const GLchar *vertexSource = "attribute vec4 position;                     \n"
                             "void main()                                  \n"
                             "{                                            \n"
                             "  gl_Position = vec4(position.xyz, 1.0);     \n"
                             "}                                            \n";
const GLchar *fragmentSource =
    "precision mediump float;\n"
    "void main()                                  \n"
    "{                                            \n"
    "  gl_FragColor[0] = gl_FragCoord.x/640.0;    \n"
    "  gl_FragColor[1] = gl_FragCoord.y/480.0;    \n"
    "  gl_FragColor[2] = 0.5;                     \n"
    "}                                            \n";

// an example of something we will control from the javascript side
bool background_is_black = true;

// the function called by the javascript code
extern "C" void EMSCRIPTEN_KEEPALIVE toggle_background_color() {
  background_is_black = !background_is_black;
}

class Game {
public:
  Game() {
    SDL_CreateWindowAndRenderer(640, 480, SDL_WINDOW_OPENGL, &window,
                                &renderer);

    // Create a Vertex Buffer Object and copy the vertex data to it
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // Create and compile the vertex shader
    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexSource, nullptr);
    glCompileShader(vertexShader);

    // Create and compile the fragment shader
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentSource, nullptr);
    glCompileShader(fragmentShader);

    // Link the vertex and fragment shader into a shader program
    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    glUseProgram(shaderProgram);

    // Specify the layout of the vertex data
    posAttrib = glGetAttribLocation(shaderProgram, "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 6,
                          (const GLvoid *)0);
  }

  void makeCurrent() { SDL_GL_MakeCurrent(window, context); }
  void swapWindow() { SDL_GL_SwapWindow(window); }

private:
  SDL_Window *window;
  SDL_Renderer *renderer;
  SDL_GLContext context;
  GLuint vertexShader, fragmentShader, shaderProgram;
  GLint posAttrib;
  GLuint vbo;
  const GLfloat vertices[6] = {0.0f, 0.5f, 0.5f, -0.5f, -0.5f, -0.5f};
};

void main_loop(void *mainloopArg) {
  Game &game = *((Game *)mainloopArg);

  game.makeCurrent();

  // Clear the screen
  glClear(GL_COLOR_BUFFER_BIT);

  // Draw a triangle from the 3 vertices
  glDrawArrays(GL_TRIANGLES, 0, 3);
  game.swapWindow();
}

int main() {
  Game game;
  void *mainLoopArg = &game;

  emscripten_set_main_loop_arg(main_loop, mainLoopArg, 0, true);

  return EXIT_SUCCESS;
}
