# Doom 3 Wasm Compilation Instructions

[Install and Activate Emscripten](https://emscripten.org/docs/getting_started/downloads.html#sdk-download-and-install)

Run:
```
emcmake cmake .
make
```

Then the run the code:
```
cd build.emscripten
python3 -m http.server 8080
```

Go to the `http://0.0.0.0:8080/doom3wasm.html`
